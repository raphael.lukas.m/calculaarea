﻿using System;
using System.Globalization;

namespace CalculaArea
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bem vindo ao programa");
            Console.WriteLine("Digite o valor do raio: ");
            double raio = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            double pi = 3.14159;
            double area = pi * (raio * raio);

            Console.WriteLine("O valor da area é: " + area.ToString("F4", CultureInfo.InvariantCulture));
            Console.ReadKey();
        }
    }
}
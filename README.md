Faça um programa para ler o valor do raio de um círculo, e depois mostrar o valor da área deste círculo com quatro 
casas decimais conforme exemplos.
Fórmula: area = pi * raio²	 

Considere o valor de π = 3.14159
